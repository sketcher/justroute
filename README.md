
![alt text](https://gitlab.com/sketcher/justroute/raw/master/example/public/assets/icon64-64.png)
## Description
> A simple PHP router for URL routing,
> with minified code for either simple applications or larger ones.
> Inspired by node-express syntax
>
> Supports regex, absolute paths and named parameters

## Installation
> require('jRoute.php')
>
> For all requests to be forced into an index file;
>
> With Apache:
>   RewriteEngine On 
>	RewriteCond %{SCRIPT_FILENAME} !-d  
>	RewriteCond %{SCRIPT_FILENAME} !-f  
>	RewriteRule ^.*$ ./index.php [L]
>
> With Nginx
>   ...  
>   root [..]/justroute/example/public;  
>   location / {  
>     	try_files $uri /index.html /index.php;  
>	    autoindex on;  
>   }  
>   ...  

## Usage
```php
$route = new jRoute;

$route::METHOD(PATH, function() {
    //... Execute code here
}, ['ALLOWED_GROUPS'...]);

$route::route( ['ACCESS_GROUPS'] );
```

## Example
```php
$route = new jRoute;

$route::get('/home',function($req){
        echo `<h2>Home page</h2>
            <p>recommended to use template views instead of this`;
});

$route::post('/image/:filename',function($req, $res){
        $res->json(
            [
                'success':true,
                'message':"uploaded $req->filename"
            ]
        )
},['member', 'admin']);

/**
 *  note: Regex-string must be ended with a $ for now. Otherwise it goes wild and latches onto everything
 */
$route::get('/^home[\/]mypage$/im',function($req){
    echo "Regex string is a match, serving 'mypage'";
},[]);

$route::setCode(404,function(){
    echo "404 Not Found (c) <== custom 404 error";
});

$route::route( ['public'] );
```
