<?php
## Require the static class
require_once(ROOT_PATH . 'jRoute.php');       
require_once(CONTROLLER_PATH . 'TestController.php');
########## Simple usage example ##########
//
//
//      a very dirty bad boy example
//
//
##########################################

// todo; set up a workkable MVC-pattern example

########## Example with controller ##########
$index = new IndexController;
$index->route();

abstract class Controller
{
    var $path;
    var $method;
    #var $access = 0;
    var $groups = [];
}

class IndexController extends Controller
{
    public function route(){
        $route = new jRoute;
        /**
         * Middleware
         */
        $route::use(function($req){
            $req->date = date('Y-m-d H:m:s');
            $req->rnd = mt_rand() / mt_getrandmax();
            $req->start = microtime(true);
            $req->ACCESS_GROUPS = ['public', 'moderator_lvl1'];
            $req->testFunction = function($r,$arg){
                echo  $arg + $r->rnd;
            };
        },function($req){
            //Custom functions here
        });
        //$route::setOptions(['ALLOWED_METHODS' => ['GET', 'POST', 'UPDATE', 'DELETE', 'PUT', 'OPTIONS']]);

        // GET
        $route::get('/',function($req, $res){
            include_once('./../view/template.php');
        },[]);

        // Controller funneled
        $route::get('/test',"TestController@indexx",[]);
        $route::get('/:xxx/abc',function($req, $res){
            $res->write('Wrong');
        },[]);
        $route::get('/test/abc',function($req, $res){
            $res->write('Correct');
        },[]);

        // GET json
        $route::get('/json',function($req, $res){
            $res->json(['success' => true, 'data' => [1, 2, 3]]);
        },[]);

        // UPDATE 
        $route::update('/',function($req, $res){
            $res->write("Path: $req->path </br> Loading took: " . measurePerformence($req->start) . "ms");
        },[]);

        // REGEX example of finding supplied adress with the incorrect casing and also ignoring file ending
        // www.example.com/MyFiles.php ==(into)==> www.example.com/myfiles
        $route::get('/[\/]myfiles$|[\/]myfiles\.\w{2,3}$/im',function($req, $res){
            $res->write( "Regex string is a match <br>" );
            var_dump($req);
        },[]);

        // -//-
        $route::get('test',function($req, $res){
            $res->write('test');
        },[]);

        // GET www.example.com/Brazil/tree/3/banana
        $route::get('/:source/:plant/:amount/:fruit',function($req, $res){
            $fruit = $req->params['fruit'];
            $amount = $req->params['amount'];
            $plant = $req->params['plant'];
            $source = $req->params['source'];
            $res->write("Picked $amount $fruit from a $plant which belongs to $source");
        },[]);

        // POST www.example.com/message
        $route::post('/message',function($req, $res){
            $res->write( "Path: $req->path </br> Method: $req->method" );
        },0,[]);

        // POST www.example.com/etc/add/something
        $route::post('/etc/:itemA/:itemB',function($req, $res){
            // $res->write($this->get_folders() + "{$req->params['itemA']}ed {$req->params['itemA']} into etc" );
        },[]);

        /*
        *
        */
        $route::get('/image',function($req, $res){
            $res->image('./assets/icon.png');
        },[]);

        /*
        *   Protected view
        */
        $route::get('/adm',function($req, $res){
            $res->write( "Protected view here" );
        },['admin','moderator']);

        /*
        *   Temp favicon fix, 
        *   todo; repalce with better exampe
        */
        $route::get('/favicon',function($req, $res){
            $res->image('./assets/favicon-16x16.png');
        },[]);

        
        /*
        *   Error codes
        */
        $route::setCodeResponse(403,function($req, $res){
            $userGroups = json_encode($req->ACCESS_GROUPS);
            $res->write("You are only <i style='color:red;'>$userGroups</i> Lacking authentication >:(");
        });
        $route::setCodeResponse(404,function($req, $res){
            $res->write( "Not Found (c)");
        });
        $route::setCodeResponse(405,function($req, $res){
            $res->write( 'Method Not Allowed :O');
        });        
        $route::setCodeResponse(204,function($req, $res){
            $res->write( 'No Content :?');
        });
        $route::setCodeResponse(500,function($req, $res){
            $res->write( 'Server Error 500');
        });


        ############################################################
        $route::route();    ## The router is executed down here
        ############################################################
    }
}

##########################################
#   Example functions below
##########################################
// ...
function measurePerformence($start){
    $end = microtime(true);
    $res = (($end - $start)*1000);
    return number_format((float)$res, 2, '.', '');
}





