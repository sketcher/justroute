<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>just route</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
    <style>
    * { font-family: verdana; font-size: 10pt; COLOR: gray; }
	body{ margin:0;padding:0; }
	b { font-weight: bold; }
	table { height: 50%; border: 1px solid gray;}
	td { text-align: center; padding: 25;}
	.root { min-height: 100vh;}
    .lc {
      position: fixed;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }
    .lc > * {
    
    }
    
    </style>
</head>
<body>

<div class="lc">
    <div>

        <?php
            echo "Path:  $req->path</br> Loading took: " . measurePerformence($req->start) . "ms</br></br>";
            echo "Query Parameters[{$req->queryParameterCount}]:<br>";
            foreach($req->params as $key => $val){
                echo "<dd>$key : $val<br>";
            }
            echo "URI Parameters[{$req->uriParameterCount}]:<br>";
            foreach($req->query as $key => $val){
                echo "<dd>$key : $val<br>";
            }
        ?>
        
    </div>
</div>

</body>
</html>