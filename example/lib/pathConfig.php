<?php
// Set folder with name "justroute" as root dir. Or fallback 2 folders up in case directory is renamed
try{
    $rootPath = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../..');
    $controllerPath = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../../example/controller');
    $examplePath = realpath(__DIR__ . DIRECTORY_SEPARATOR . '../../example');
    defined('ROOT_PATH') or define('ROOT_PATH', $rootPath  . DIRECTORY_SEPARATOR); 
    defined('CONTROLLER_PATH') or define('CONTROLLER_PATH', $controllerPath  . DIRECTORY_SEPARATOR); 
    defined('EXAMPLE_PATH') or define('EXAMPLE_PATH', $examplePath  . DIRECTORY_SEPARATOR . 'controller/index.php'); 

    /*
    *   todo; Set something environment-safe up
    */

}
catch(Exception $e){
    // -//- ... 
}


